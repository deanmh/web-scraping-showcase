# Dean Mehinovic, 2009. All rights reserved.
# Log into the Apexedi account and fetch the data of all medical insurance 
# claims not found in the local master file containing the names of all 
# previously retrieved claims.

require 'rubygems'
require 'watir'
require 'hpricot'
require 'fastercsv'

# Use Hpricot to parse out the webpage of a given claim. Store the relevant 
# data fields in a csv file.
 
def parse(claim, filename) 
  doc = Hpricot(claim)
  count = 0                  
  headers = ["Claimid", "Lastname", "Firstname", "Company", "DOS", "Amount", 
             "Status"]
  master = []
  master << headers
  row = []
    
  (doc/"p.tbody").each do |elem|
    if !(elem.inner_html =~ /Delete/) or elem.inner_html =~ /Deleted/
      count += 1
      
      if count > 6 
        if count % 6 == 2
          row << elem.inner_html.gsub(/(<.*?>)|(&nbsp;)/, "").split(', ')[0]
          row << elem.inner_html.gsub(/(<.*?>)|(&nbsp;)/, "").split(', ')[1]
        else
          row << elem.inner_html.gsub(/(<.*?>)|(&nbsp;)/, "")
        end
      end
      
      if count > 11 && count % 6 == 0
        master << FasterCSV::Row.new(headers, row)
        row = Array.new
      end  
    end
  end
  
  FasterCSV.open(filename+".csv", "w") do |csv|
    master.each do |row|
      csv << row
    end
  end
end  

# Use a Watir virtual browser to log into the Apexedi website.
ie = Watir::IE.start("http://www.apexedi.com/")
ie.text_field(:name, "username").set("username_redacted")
ie.text_field(:name, "password").set("password_redacted")
ie.form(:name, "loginForm").submit
ie.links[6].click

# Read the previously retrieved claims' names from file.
list = ""
if File.exist?("retrieved.txt")
  list = IO.read("retrieved.txt")
end

# For all the claims that haven't yet been retrieved, parse their webpages and 
# store their data locally.
File.open("retrieved.txt", "a") do |f|
  list = list.scan(/\S+/)
  ie.links.each do |link|
    puts link
    if link.text =~ /\d{8,8}\D{3,3}/
      if list.select{|e| e == link.text }.empty?
        f << link.text+"\n"
        filename = link.text
        link.click
        parse(ie.html, filename)
        ie.back
      else
        break
      end
    end
  end  
end

ie.close
