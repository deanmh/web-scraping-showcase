# Dean Mehinovic, 2009. All rights reserved.
# Decrypt and retrieve claims data and use them to fill out and submit a Capital
# One dental fee plan form. 

require 'rubygems'
require 'watir'
require 'pstore'
require 'openssl'
require 'yaml'

# Use OpenSSL to decrypt medical claims data that were stored and encrypted 
# using a randomly generated AES-256 key, which was then encrypted using RSA. 
# 'encrypted_iv' represents the initialization vector. 

def decrypt(encrypted_key, encrypted_iv, encrypted_data, private_key)
  cipher = OpenSSL::Cipher::Cipher.new('aes-256-cbc')   
  cipher.decrypt   

  cipher.key = private_key.private_decrypt(encrypted_key)   
  cipher.iv = private_key.private_decrypt(encrypted_iv)   

  decrypted_data = cipher.update(encrypted_data)   
  decrypted_data << cipher.final

  return decrypted_data
end

# Fill the appropriate fields from the decrypted claims data and submit the 
# form. 

def submit_form(input)
  form_url = "https://secure.capitalonefinance.com/form.asp?plan=DFP&currPage=0" 
  ie = Watir::IE.start(form_url)
  
  ie.text_field(:name, "Applicant/Treatment/Amount").set("2000")
  ie.select_list(:name , \
                        "Applicant/ApplicantLocation").select("Doctor's Office")
  
  ie.select_list(:name , "Applicant/ApplierType").select( \
                            "A staff member applying on behalf of an Applicant")
  
  ie.checkbox(:name, "Applicant/HasPermissionToApply").set
  ie.checkbox(:name, "Applicant/DiscloseToProvider").set
  ie.button(:name, "yesDoc").click
  
  ie.text_field(:name, \
          "Applicant/DoctorInfo/Search/Params/InOffice/ByZIP/Phone1").set("203")
  ie.text_field(:name, \
          "Applicant/DoctorInfo/Search/Params/InOffice/ByZIP/Phone2").set("555")
  ie.text_field(:name, \
         "Applicant/DoctorInfo/Search/Params/InOffice/ByZIP/Phone3").set("5555")
  ie.button(:name, "cont").click
  
  ie.radio(:name, "Applicant/DoctorInfo/DoctorID").click
  ie.button(:name, "yesDoc").click
  
  r_hash = {"1" => "550", "2" => "643", "3" => "283", "4" => "672", "5" => "686"}

  # Select 36 months, the default repayment period.
  choice = 3
  unless choice <= ie.radios.length 
    choice = ie.radios.length.to_s 
  end
    
  ie.radios[choice].click
  ie.button(:name, "cont").click  
  
  textfields_list = ["Applicant/Name/First", "Applicant/Name/MI",
                  "Applicant/Name/Last", "Applicant/SSN1", "Applicant/SSN2","Applicant/SSN3", "Applicant/DOB3", "Applicant/Email","Applicant/ContactInfo/Street1", "Applicant/ContactInfo/City","Applicant/ContactInfo/ZIP", 
                  "Applicant/ContactInfo/HomePhone1",
                  "Applicant/ContactInfo/HomePhone2", 
                  "Applicant/ContactInfo/HomePhone3",
                  "Applicant/HousingPayment", 
                  "Applicant/EmploymentInfo/Employer",  "Applicant/EmploymentInfo/Position",
                  "Applicant/EmploymentInfo/Phone1",
                  "Applicant/EmploymentInfo/Phone2", 
                  "Applicant/EmploymentInfo/Phone3","Applicant/EmploymentInfo/Extension", "Applicant/GrossIncome"] 
  dropdowns_list = ["Applicant/DOB1", "Applicant/DOB2", 
                    "Applicant/ContactInfo/State", 
                    "Applicant/ContactInfo/OwnRent","Applicant/ContactInfo/MailingSameAsPhysical",
                    "Applicant/EmploymentStatus", "Applicant/Patient1Type","Applicant/Patient1/IsUnder18", "Applicant/TreatmentTypes"]
  
  textfields_list.each do |textfield_name|
    ie.text_field(:name, textfield_name).set(input[textfield_name].to_s)
  end
  
  dropdowns_list.each do |dropdown_name|
    ie.select_list(:name , dropdown_name).select(input[dropdown_name].to_s)
  end
  
  ie.button(:name, "cont").click
  
  ie.text_field(:name, "Applicant/ConfirmEmail").set( \
                                                 input["Applicant/Email"].to_s)
  ie.select_list(:name , "Applicant/WhereToContact").select(input["Applicant/WhereToContact"].to_s)
  ie.select_list(:name , "Applicant/HowFound").select("Through Doctor")  
  
  ie.checkbox(:name, "Agree").set
  ie.button(:name, "finish").click
end

# Use a previously created RSA private key to decrypt previously encrypted
# claims data. Retrieve the data from format and fill out and submit the 
# Capital One form.
private_key_file = 'private.pem';   
private_key = OpenSSL::PKey::RSA.new(File.read(private_key_file), \
                                               "password_redacted")   
user_data = ""
encrypted_db = PStore.new("capital_one_forms")

encrypted_db.transaction do
  encrypted_db.roots.each do |data_root_name|
    encrypted_key = encrypted_db[data_root_name]["encrypted_key"] 
    encrypted_iv = encrypted_db[data_root_name]["encrypted_iv"] 
    encrypted_data = encrypted_db[data_root_name]["encrypted_data"] 
    user_data = decrypt(encrypted_key, encrypted_iv, \
                        encrypted_data, private_key)
    
    deyamlized_data = YAML::load(user_data)
    submit_form(deyamlized_data)
  end
end
 